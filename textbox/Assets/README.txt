
~~ Taking the Textbox ~~

To use the textbox in your game, you will need the following items:
	- Prefabs > Textbox
	- Scripts > Textbox Scripts > Textbox, EventManager, and EventController
	- Hierarchy > Event Manager (this is just an empty GameObject which is needed to hold the EventManager script)
	- after copying these into your project, drag the textbox object into the EventManager script in the inspector
	- the input button "Continue Dialogue" must be created in Edit > Project Settings > Input


~~ Taking the Options ~~

You may also take the options object if you wish (the UIO icon that appears above the Villager's head). If so, you will need:
	- Prefabs > UIO Sensor
	- Scripts > Option Scripts > OptionInput, OptionSensor
	- a player GameObject which is tagged with "Player" (for the sensor to activate/deactivate the options GameObject)
	- the input buttons must be set in Edit > Project Settings > Input


<<NOTE>>
If you do not use the options object, you will need some other way of calling the functions in the EventManager script



~~ Using the Textbox ~~

- EventManager is the script I used for calling the textbox functions, but if you want to make a new script which will use the code, simply make the
  script inherit from Textbox and then set up the public variables in the inspector by draggin the textbox (GameObject) into the first field and selecting
  the fourth animationCurve preset graph and setting it to pingpong (by clicking on the gear at the top of the graph)
- If you create a new script, you must also set Start and Update to protected override instead of private and put base.Start(); and base.Update(); inside
  their corresponding methods
- You can easily change the appearance of the textbox by changing the sprites in its children's SpriteRenderers
- If you want to activate/deactivate scripts or booleans such player controls while the textbox is open, include these lines of code in lines 50 and 60
  in the textbox script, as well as 71 in the EventController script



~~ Using the Options ~~

- Both scripts are used in the prefab in a way so that everything can be altered inside the inspector and the scripts just have to exist somewhere in the 
  Assets folder
- In each Option GameObject inside the prefab, you will have to set the Button name for the corresponding option (by default they are "Option 1," 
  "Option 2," and "Option 3" but they must still be set in the Input menu)
- In each Option GameObject (in the scene), there will be an event ready to be setup by simply targetting an object and a function from one of its scripts 
  (I used an Event Manager which holds all the functions together)
- Whenever the specified button is pressed while the object is active, all functions in the event will run