﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OptionEvent : UnityEvent { }

public class OptionInput : MonoBehaviour
{
    public OptionEvent optionEvent;
    public string buttonName;
	public static bool scriptActive = true;

    private void Update() {
        if (Input.GetButtonDown(buttonName) && scriptActive) {
            optionEvent.Invoke();
        }
    }
}
