﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionSensor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player") {
            GameObject[] otherOptions = GameObject.FindGameObjectsWithTag("UIO");
            for (int i=0; i<otherOptions.Length; i++) {
                otherOptions[i].SetActive(false);
            }
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Player") {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
