﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : Textbox
{
	public GameObject dragon;

	protected override void Start() {
		base.Start();
	}
	protected override void Update() {
		base.Update();
	}

	//the functions available to you are MoveObject(), CameraShake()/Shake(), AddMethod(), and OpenTextbox()


	//here are some examples of my code which is triggered with the Option 1/2/3 GameObjects from the UIO Sensor prefab under the villager

	public void SpeakToVillager() {
		AddMethod(0, delegate { OpenTextbox("Villager", "Oh you made it back!"); });
		AddMethod(0, delegate { dragon.SetActive(true); });
		AddMethod(0, delegate { CameraShake(0, 0.6f, 0.2f); });
		AddMethod(0.2f, delegate { CameraShake(0.6f, 1.5f); });
		AddMethod(0, delegate { MoveObject(MovementType.displace, dragon, new Vector3(0,45,0), 1.5f); });
		AddMethod(0, delegate { dragon.SetActive(false); });
		AddMethod(0, delegate { dragon.transform.position = initialMovementPos; });
		AddMethod(0, delegate { CameraShake(0.6f,0,0.5f); });
		AddMethod(1, delegate { OpenTextbox("Villager","What was that?!"); });
	}

    public void AttackVillager() {
        AddMethod(0, delegate { OpenTextbox("Villager", "OWIE!!!_Don't do that!"); });
		AddMethod(0, delegate { Shake(GameObject.Find("Villager"), 0, 0.8f, 1.2f); });
		AddMethod(1.2f, delegate { MoveObject(MovementType.boomerang, GameObject.Find("Villager"),GameObject.Find("Player").transform.position,0.13f); });
		AddMethod(0, delegate { GameObject.Find("Player").GetComponent<DamageBehaviour>().health = 0; });
		AddMethod(2, delegate { OpenTextbox("Villager", "Oops my punch was so strong you died..._My bad."); });
    }

    public void FlirtWithVillager() {
		OpenTextbox("Lovely Women","What a hotty *sigh*_Wait..._Did you hear that?_Oh my gosh_I feel so embarrased_UwU");
    }
}
