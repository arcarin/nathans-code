﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void AddedMethod();
public enum MovementType { linear, boomerang, displace }

public class EventController : MonoBehaviour {
	[Space]
	[Header("Controller")]
	private List<AddedMethod> methods = new List<AddedMethod>();
	private List<float> delays = new List<float>();
	protected static bool pauseDelays = false;
	private float delayStart;
	public GameObject textboxObject;
	private bool finishedMethods = true;

	[Space]
	[Header("Movement")]
	private GameObject lerpUser;
	private MovementType movementType;
	protected Vector3 initialMovementPos;
	private Vector3 finalMovementPos;
	private float movementDuration;
	public AnimationCurve boomerangCurve;
	private float movementTimerStart;
	private float movementTimerRun;
	private bool movementFinished = true;

	[Space]
	[Header("Camera Shake")]
	private GameObject shakeObject;
	private Vector3 shakeInitialPos;
	private float initialShakeIntensity;
	private float finalShakeIntensity;
	private float shakeDuration;
	protected float shakeTimerStart;
	private float shakeTimerRun;
	private bool shakeFinished = true;

	protected virtual void Start() {
		movementDuration = 0;
		movementTimerStart = Time.time - 2;
	}

	protected virtual void Update() {

		//methods
		if (methods.Count > 0) {
			bool stopLooping = false;
			while (!pauseDelays && methods.Count > 0 && !stopLooping) {
				if (delays[0] <= 0) {
					methods[0]();
					methods.RemoveAt(0);
					delays.RemoveAt(0);
					if (methods.Count > 0) {
						delayStart = Time.time + delays[0];
					}
				} else {
					stopLooping = true;
				}
			}
		}
		if (methods.Count > 0) {
			if (!pauseDelays) {
				delays[0] = delayStart - Time.time;
			} else {
				delayStart = Time.time + delays[0];
			}
		} else if (!finishedMethods && !textboxObject.activeInHierarchy) {
			/*/activate scripts/*/
			finishedMethods = true;
		}

		//MoveObject
		movementTimerRun = Time.time - movementTimerStart;
		if (movementTimerRun < movementDuration) {
			if (movementType == MovementType.linear || movementType == MovementType.displace) {
				lerpUser.transform.position = Vector3.Lerp(initialMovementPos, finalMovementPos, movementTimerRun / movementDuration);
			} else if (movementType == MovementType.boomerang) {
				lerpUser.transform.position = Vector3.Lerp(initialMovementPos, finalMovementPos, boomerangCurve.Evaluate(movementTimerRun / movementDuration * 2));
			}
			pauseDelays = true;
		} else if (!movementFinished) {
			if (movementType == MovementType.boomerang) {
				lerpUser.transform.position = initialMovementPos;
			}
			pauseDelays = false;
			movementFinished = true;
		}

		//CameraShake
		shakeTimerRun = Time.time - shakeTimerStart;
		if (shakeTimerRun < shakeDuration) {
			float currentShakeIntensity = Mathf.Lerp(initialShakeIntensity,finalShakeIntensity,shakeTimerRun/shakeDuration);
			Vector3 nextPosition = Random.insideUnitCircle * currentShakeIntensity;
			shakeObject.transform.position = nextPosition + shakeInitialPos;
			finishedMethods = true;
		} else if (!shakeFinished) {
			shakeObject.transform.position = shakeInitialPos;
			shakeFinished = true;
			finishedMethods = false;
		}
	}

	protected void AddMethod(float delay, AddedMethod method) {
		delays.Add(delay);
		methods.Add(method);
		delayStart = Time.time + delays[0];
		finishedMethods = false;
	}

	protected void MoveObject(MovementType type, GameObject targetObject, Vector3 targetPosition, float elapsedTime) {
		movementType = type;
		lerpUser = targetObject;
		initialMovementPos = targetObject.transform.position;
		finalMovementPos = new Vector3(targetPosition.x, targetPosition.y, targetObject.transform.position.z);
		movementDuration = elapsedTime;
		movementTimerStart = Time.time;
		movementFinished = false;
		pauseDelays = true;
		if (type == MovementType.displace) {
			targetObject.transform.Translate(targetPosition);
			finalMovementPos = targetObject.transform.position;
			targetObject.transform.Translate(-targetPosition);
		}
	}

	protected void CameraShake(float intensity, float duration) {
		shakeObject = Camera.main.gameObject;
		initialShakeIntensity = intensity;
		finalShakeIntensity = intensity;
		shakeDuration = duration;
		shakeInitialPos = shakeObject.transform.position;
		shakeTimerStart = Time.time;
		shakeFinished = false;
	}
	protected void CameraShake(float initialIntensity, float finalIntensity, float duration) {
		shakeObject = Camera.main.gameObject;
		initialShakeIntensity = initialIntensity;
		finalShakeIntensity = finalIntensity;
		shakeDuration = duration;
		shakeInitialPos = shakeObject.transform.position;
		shakeTimerStart = Time.time;
		shakeFinished = false;
	}

	protected void Shake(GameObject targetObject, float intensity, float duration) {
		shakeObject = targetObject;
		initialShakeIntensity = intensity;
		finalShakeIntensity = intensity;
		shakeDuration = duration;
		shakeInitialPos = shakeObject.transform.position;
		shakeTimerStart = Time.time;
		shakeFinished = false;
	}
	protected void Shake(GameObject targetObject, float initialIntensity, float finalIntensity, float duration) {
		shakeObject = targetObject;
		initialShakeIntensity = initialIntensity;
		finalShakeIntensity = finalIntensity;
		shakeDuration = duration;
		shakeInitialPos = shakeObject.transform.position;
		shakeTimerStart = Time.time;
		shakeFinished = false;
	}
}
