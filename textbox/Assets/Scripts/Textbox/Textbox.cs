﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Textbox : EventController
{
    protected int maxTextLength = 57;
    private TextMesh textDisplay;
    private TextMesh nameDisplay;
	private List<string> texts = new List<string>();
	private int timer = 0;
	private int currentText = 0;

	protected override void Start() {
		base.Start();
		textDisplay = textboxObject.transform.GetChild(1).GetComponent<TextMesh>();
		nameDisplay = textboxObject.transform.GetChild(3).GetComponent<TextMesh>();
	}

    protected override void Update() {
		base.Update();
		if (textboxObject.activeInHierarchy)
        {
            //increase the timer to make the bululululululu effect
            if (timer < texts[currentText].Length)
            {
                timer++;
            }
            //display the text according to the timer
            textDisplay.text = texts[currentText].Substring(0, timer);

            //run input controls
            if (Input.GetButtonDown("Continue Dialogue"))
            {
                if (timer < texts[currentText].Length)
                {
                    //quickly skip through the bululululululu
                    timer = texts[currentText].Length;
                }
                else if (currentText < texts.Count - 1)
                {
                    //go to the next page of text
                    currentText++;
                    timer = 0;
                }
                else
                {
                    //close the textbox and play the next method in EventController
                    textboxObject.SetActive(false);
					/*/activate scripts/*/
					pauseDelays = false;
                }
            }
        }
    }

    protected void OpenTextbox(string characterName, string text)
    {
		//reset textbox variables
		/*/deactivate scripts/*/
		textboxObject.SetActive(true);
        currentText = 0;
        timer = 0;
		pauseDelays = true;

        nameDisplay.text = characterName;
        texts.Clear();

        //loop the text and trigger the next text boxes

        List<string> temporaryTexts = new List<string>();
        int temporaryLength = 0;
        while (text.Length - temporaryLength > maxTextLength)
        {
            string temporaryText = text.Substring(temporaryLength, maxTextLength);

            //skip to the next page if the _ key is found in the string
            bool skipText = false;
            for (int i = 0; i < temporaryText.Length; i++)
            {
                if (temporaryText[i] == '_' && !skipText)
                {
                    skipText = true;
                    temporaryText = temporaryText.Substring(0, i);
                }
            }

            //loop backwards until it reaches the end of a word before ending the line
            while (temporaryText.Length > 0 && temporaryText[temporaryText.Length - 1] != ' ' && !skipText)
            {
                temporaryText = temporaryText.Substring(0, temporaryText.Length - 1);
            }

            //save the temporary text to the list
            temporaryTexts.Add(temporaryText);
            temporaryLength += temporaryText.Length;

            //is skipText is true, delete the underscore and adjust the rest of the page
            if (skipText)
            {
                temporaryLength++;
                if (temporaryTexts.Count % 2 == 1)
                {
                    temporaryTexts.Add("");
                }
            }
        }
        //add any remaining lines of text
        if (text.Length > temporaryLength)
        {
            List<string> remainingText = new List<string>();
            remainingText.Add(text.Substring(temporaryLength, text.Length - temporaryLength));

            //skip to the next page if the _ key is found in the string
            bool skipText = true;
            int latestText = 0;
            while (skipText)
            {
                skipText = false;
                for (int i = 0; i < remainingText[latestText].Length; i++)
                {
                    if (remainingText[latestText][i] == '_' && !skipText)
                    {
                        skipText = true;
                        remainingText.Add("");
                        remainingText.Add(remainingText[latestText].Substring(i + 1, remainingText[latestText].Length - i - 1));
                        remainingText[latestText] = remainingText[latestText].Substring(0, i);
                        latestText += 2;
                    }
                }
            }

            //add the remainingText to the temporaryTexts
            for (int i = 0; i < remainingText.Count; i++)
            {
                temporaryTexts.Add(remainingText[i]);
            }
        }

        //add the temporaryTexts list to the main list
        while (temporaryTexts.Count >= 2)
        {
            if (!temporaryTexts[1].Equals(""))
            {
                texts.Add(temporaryTexts[0] + "\n" + temporaryTexts[1]);
            }
            else
            {
                texts.Add(temporaryTexts[0]);
            }
            temporaryTexts.RemoveAt(0);
            temporaryTexts.RemoveAt(0);
        }
        if (temporaryTexts.Count > 0)
        {
            texts.Add(temporaryTexts[0]);
        }

    }
}
