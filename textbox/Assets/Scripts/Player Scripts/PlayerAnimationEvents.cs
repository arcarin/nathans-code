﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AttackEvent : UnityEvent<Vector3, float, float> { }
//paramerters are: position enemy is knocked back from, knockback strength, damage dealt

public class PlayerAnimationEvents : MonoBehaviour
{
	public PlayerControls controlsScript;
	public PlayerAttackField attackFieldScript;
	public AttackEvent attackEvent;

	public void Attack()
	{
		for (int i = 0; i < attackFieldScript.enemyScriptsInRange.Count; i++) {
			attackEvent.AddListener(attackFieldScript.enemyScriptsInRange[i].TakeDamage);
		}
		attackEvent.Invoke(transform.position, controlsScript.knockBackStrength, controlsScript.attackStrength);
		attackEvent.RemoveAllListeners();
	}

	public void EndAttack()
    {
		controlsScript.attackCooldownStart = Time.time;
		controlsScript.canAttack = true;
    }
}
