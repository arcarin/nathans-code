﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBehaviour : MonoBehaviour {
	public float health;
	public bool invincible = false;
	public Animator animator;
	private Rigidbody2D rb;


	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		perish();
	}

	public void TakeDamage(Vector3 hitPosition, float knockBackStrength, float damage)
	{
		if (!invincible) {
			Vector3 directionFromHit = (transform.position - hitPosition).normalized;
			rb.AddForce(directionFromHit * knockBackStrength, ForceMode2D.Impulse);
			health -= damage;
		}
	}

	void perish()
	{
		if (health <= 0 && gameObject.tag != "Player") {
			Destroy(gameObject);
		} else if (health <= 0 && gameObject.tag == "Player") {
			GetComponent<PlayerControls>().enabled = false;
			enabled = false;
			animator.SetTrigger("death");
		}
	}
}
