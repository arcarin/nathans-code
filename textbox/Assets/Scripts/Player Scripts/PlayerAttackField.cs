﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackField : MonoBehaviour
{
	public List<DamageBehaviour> enemyScriptsInRange = new List<DamageBehaviour>();

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Enemy") {
			enemyScriptsInRange.Add(collision.GetComponent<DamageBehaviour>());
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Enemy") {
			enemyScriptsInRange.Remove(collision.GetComponent<DamageBehaviour>());
		}
	}
}
