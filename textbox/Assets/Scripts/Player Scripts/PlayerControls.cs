﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerControls : MonoBehaviour
{
    [Space]
    [Header("Components")]
    private Rigidbody2D rb;
    public Animator spriteAnimator;
	private DamageBehaviour damageScript;

	[Space]
	[Header("GameObjects")]
	public GameObject jumpableObjects;

	[Space]
    [Header("Movement Variables")]
    public float speed;
    public float maxSpeed;
	private float horizontalScale;

    [Space]
    [Header("Jump Variables")]
    public float jumpHeight;
    public float jumpLength;
	public float invincibilityHeight;
	public float jumpCooldown;
    public AnimationCurve jumpCurve;
    public float jumpTimerStart;
    public float jumpTimerRun;
	private bool endedJump = true;

	[Space]
	[Header("Attack Variables")]
	public float attackStrength;
	public float knockBackStrength;
	public float attackCooldownLength;
	public float attackCooldownStart;
	private float attackCooldownRun;
	public bool canAttack = true;


	[Space]
    [Header("Input Variables")]
    private float horizontalInput;
    private float verticalInput;
	public static bool scriptActive = true;
	public static bool activateScript = false;



	void Start() {
        //Initialize variables
        jumpTimerStart = -jumpLength - jumpCooldown;
		attackCooldownStart = -attackCooldownLength;
		horizontalScale = transform.localScale.x;

        //Get components
        rb = GetComponent<Rigidbody2D>();
		damageScript = GetComponent<DamageBehaviour>();
    }
    
    void Update() {
        //set input variables
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

		//call functions
		if (scriptActive) {
			PlayerJump();
			PlayerAttackStart();
		}
		SetAnimatorParameters();
		FaceLeft();
		if (activateScript && Input.GetButtonUp("Continue Dialogue")) {
			scriptActive = true;
			activateScript = false;
		}
	}

    private void FixedUpdate() {
		//move player and then clamp velocity
		if (scriptActive) {
			MovePlayer();
		}
    }


    //FUNCTIONS//

    private void MovePlayer() {
        //move player and then clamp velocity
        rb.AddForce(new Vector2(horizontalInput, verticalInput) * speed);
        if (rb.velocity.magnitude > maxSpeed) {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
    }

    private void PlayerJump() {
        //activate jump on button press
        if (Input.GetButton("Jump") && jumpTimerRun > jumpLength + jumpCooldown) {
            jumpTimerStart = Time.time;
			endedJump = false;
			//allow attacks to be used in case an attack was cancelled by a jump
			canAttack = true;
		}
		//run jump timer
		jumpTimerRun = Time.time - jumpTimerStart;
		//use a lerp to create the movement for the jump (by displacing the "Jump Objects" child object)
		if (jumpTimerRun / jumpLength * 2 <= 2) {
			Vector3 maxHeightPosition = new Vector3(transform.position.x, transform.position.y + jumpHeight, transform.position.z);
            jumpableObjects.transform.position = Vector3.Lerp(transform.position, maxHeightPosition, jumpCurve.Evaluate(jumpTimerRun / jumpLength * 2));
		}
		//reset sprite position on the last frame of the jump
		if (jumpTimerRun / jumpLength * 2 > 2 && !endedJump){
			endedJump = true;
			jumpableObjects.transform.position = transform.position;
		}
		//make player invincible once they reach the specified height while jumping
		if (jumpTimerRun / jumpLength * 2 > invincibilityHeight/jumpHeight && jumpTimerRun / jumpLength * 2 < 2 - invincibilityHeight / jumpHeight) {
			damageScript.invincible = true;
		} else {
			damageScript.invincible = false;
		}
	}

	private void PlayerAttackStart() {
		//make player attack when the attack button is pressed and player is not jumping
		attackCooldownRun = Time.time - attackCooldownStart;
		if (Input.GetButton("Attack") && canAttack && attackCooldownRun > attackCooldownLength && jumpTimerRun / jumpLength * 2 > 2) {
			spriteAnimator.SetTrigger("attack");
			canAttack = false;
		}
	}

	private void SetAnimatorParameters() {
		//set animator parameters
		spriteAnimator.SetFloat("speed", Mathf.Abs(rb.velocity.magnitude));
		spriteAnimator.SetFloat("jumpTime", jumpTimerRun / jumpLength * 2);
	}

	private void FaceLeft() {
		//make player face left when velocity is negative
		if (rb.velocity.x > 0) {
			transform.localScale = new Vector3(horizontalScale, transform.localScale.y, 1);
		} else if (rb.velocity.x < 0) {
			transform.localScale = new Vector3(-horizontalScale, transform.localScale.y, 1);
		}
	}
}
