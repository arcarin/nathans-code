﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAttackField : MonoBehaviour
{
	public NPCBehaviour behaviourScript;
	
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject == behaviourScript.target) {
			behaviourScript.inAttackingRange = true;
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject == behaviourScript.target) {
			behaviourScript.inAttackingRange = false;
		}
	}
}