﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCTargettor : MonoBehaviour
{
	public NPCBehaviour behaviourScript;
	private string parentTag;

	private void Start() {
		parentTag = transform.parent.transform.gameObject.tag;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if ((parentTag == "Enemy" && (collision.tag == "Player" || collision.tag == "Ally")) || (parentTag == "Ally" && (collision.tag == "Enemy"))) {
			if (behaviourScript.target == null) {
				behaviourScript.target = collision.gameObject;
				behaviourScript.attackTarget.AddListener(behaviourScript.target.GetComponent<DamageBehaviour>().TakeDamage);
			}
		}
	}
}
