﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AttackTarget : UnityEvent<Vector3, float, float> { }
//paramerters are: position enemy is knocked back from, knockback strength, damage dealt

public class NPCBehaviour : MonoBehaviour 
{
	public static bool scriptActive = true;

	[Space]
	[Header("Unhostile Movement Variables")]
	public float unhostileSpeed;
	private Vector3 targetPosition;
	public float minTravelRange, maxTravelRange;
	private Vector3 spawnPosition;
	public float maxRangeFromSpawn;
	public float movementDelayMin, movementDelayMax;
	private float movementStart;
	private float movementRun;

	[Space]
	[Header("Hostile Movement Variables")]
	public float speed;
	public GameObject target = null;
    public Transform uio;
	private float horizontalScale;

	[Space]
	[Header("Attack Variables")]
	public float attackStrength;
	public float knockbackStrength;
	public float attackCooldown;
	public float attackCooldownStart;
	private float attackCooldownRun;
	public bool inAttackingRange = false;
	public AttackTarget attackTarget;

	void Start()
    {
		//initialize variables
		movementStart = Time.time + Random.Range(movementDelayMin, movementDelayMax);
		targetPosition = Random.insideUnitCircle * maxTravelRange + new Vector2(transform.position.x, transform.position.y);
		spawnPosition = transform.position;
		horizontalScale = transform.localScale.x;
    }
	
    void Update()
    {
		if (scriptActive) {
			UnhostileMovement();
			HostileMovement();
			Attack();
		}
    }
	
	void UnhostileMovement()
	{
		if (target == null) {
			if (Vector3.Distance(targetPosition, transform.position) >= unhostileSpeed) {
				Vector3 directionToTargetPos = targetPosition - transform.position;
				transform.position += directionToTargetPos.normalized * Time.deltaTime * unhostileSpeed;
				movementStart = Time.time + Random.Range(movementDelayMin, movementDelayMax);

				if (targetPosition.x > transform.position.x) {
					transform.localScale = new Vector3(horizontalScale, transform.localScale.y, 1);
                    if (uio != null)
                        uio.localScale = new Vector3(1,1,1);
				}
				if (targetPosition.x < transform.position.x) {
					transform.localScale = new Vector3(-horizontalScale, transform.localScale.y, 1);
                    if (uio != null)
                        uio.localScale = new Vector3(-1, 1, 1);
                }
			}
			if (Vector3.Distance(targetPosition, transform.position) < unhostileSpeed) {
				movementRun = Time.time - movementStart;
			}
			if (movementRun >= 0 && Vector3.Distance(targetPosition, transform.position) < unhostileSpeed) {
				targetPosition = Random.insideUnitCircle * maxTravelRange + new Vector2(transform.position.x, transform.position.y);
				while (Vector3.Distance(targetPosition,spawnPosition) > maxRangeFromSpawn || Vector3.Distance(targetPosition,transform.position) < minTravelRange) {
					targetPosition = Random.insideUnitCircle * maxTravelRange + new Vector2(transform.position.x, transform.position.y);
				}
			}
		}
	}

	void HostileMovement() {
		if (target != null && !inAttackingRange) {
			Vector3 directionToTarget = target.transform.position - transform.position;
			transform.position += directionToTarget.normalized * Time.deltaTime * speed;

			if (target.transform.position.x > transform.position.x) {
				transform.localScale = new Vector3(horizontalScale, transform.localScale.y, 1);
                if (uio != null)
                    uio.localScale = new Vector3(1, 1, 1);
            }
			if (target.transform.position.x < transform.position.x) {
				transform.localScale = new Vector3(-horizontalScale, transform.localScale.y, 1);
                if (uio != null)
                    uio.localScale = new Vector3(-1, 1, 1);
            }
		}
	}
	
	public void Attack()
	{
		attackCooldownRun = Time.time - attackCooldownStart;
		if (inAttackingRange && attackCooldownRun > attackCooldown) {
			attackTarget.Invoke(transform.position,knockbackStrength,attackStrength);
			attackCooldownStart = Time.time;
		}
	}
}
