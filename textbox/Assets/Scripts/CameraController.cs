﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public GameObject mainCamera;
	public Vector2 direction;

	private void OnTriggerStay2D(Collider2D collision) {
		if (collision.tag == "Player") {
			Vector2 velocity = collision.GetComponent<Rigidbody2D>().velocity;
			mainCamera.transform.position += new Vector3(Mathf.Abs(velocity.x)*direction.x, Mathf.Abs(velocity.y) * direction.y, 0) * Time.deltaTime;
		}
	}
}
